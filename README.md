# Library Management

### Setup

#### Clone repository
```
git clone <CLONE_URL>
```

#### cd into directory
```
cd librarymanagement/
```

#### Create .env from .env.example
```
cp .env.example .env
```
#### create a database and put that db name in your .env
```
DB_DATABASE=your_db_name
```
#### Create a virtual host and place the url in .env
```
APP_URL=http://yourvhost.test
PASSPORT_LOGINLINK="http://yourvhost.test/oauth/token"
```
#### Migration
```
php artisan migrate
```
#### Seed
```
php artisan db:seed
```
#### OAuth
```
php artisan passport:install
```

#### You will get `Client_id` and `Client_secret` from above. Copy the password grant client id and secret and put it in .env
```
PASSPORT_CLIENT_ID= <Client_ID>
PASSPORT_CLIENT_SECRET= <Client_secret>
```
#### access the url and login with following details
```
Email- demo@demo.com
Password- demo@123
```

