<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Book;
use App\LibraryUser;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>"Demo Admin",
            'email'=>"demo@demo.com",
            'password'=>bcrypt("demo@123")
        ]);
        Book::create([
            'title'=>"Book 1",
            'isbn'=>"1234",
            'shelf_no'=>'1',
            'stock'=>100
        ]);
        Book::create([
            'title'=>"Book 2",
            'isbn'=>"12345",
            'shelf_no'=>'2',
            'stock'=>100
        ]);
        LibraryUser::create([
            'name'=>'User 1',
            'gender'=>'male',
            'email'=>'email@address.com',
            'primary_phone'=>'9999999999',
            'address_line1'=>'123-43',
            'address_line2'=>null,
            'city'=>'Delhi',
            'street'=>'Old Delhi',
            'zip'=>'110006',
            'country'=>'India'
          ]);
        LibraryUser::create([
            'name'=>'User 2',
            'gender'=>'female',
            'email'=>'email1@address.com',
            'primary_phone'=>'6666666666',
            'address_line1'=>'234/4',
            'address_line2'=>null,
            'city'=>'Delhi',
            'street'=>'Old Delhi',
            'zip'=>'110006',
            'country'=>'India'
          ]);
    }
}
