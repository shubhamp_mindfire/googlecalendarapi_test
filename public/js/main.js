
$(document).on("click",".showmorebuttons",function(){
    $(this).next().show();
    $(this).prev().hide();
    $(this).hide();
})

var elements = document.querySelectorAll('input,select,textarea');
var invalidListener = function () {
    this.scrollIntoView(false);
};

for (var i = elements.length; i--;)
    elements[i].addEventListener('invalid', invalidListener);
