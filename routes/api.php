<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', "AuthController@login");
Route::post('/register', "AuthController@register");
Route::get('/users', "LibraryController@getusers");
Route::get('/alllibraryusers', "LibraryController@alllibraryusers");
Route::post('/users', "LibraryController@store");
Route::get('/users/{id}', "LibraryController@getUserdetail");
Route::put('/users/{id}', "LibraryController@update");
Route::delete('/users/{id}', "LibraryController@destroy");
Route::post('/users/multipledelete', "LibraryController@multipleDelete");

Route::get('/books', "LibraryController@getbooks");
Route::post('/books', "LibraryController@storebook");
Route::get('/books/{id}', "LibraryController@getBookdetail");
Route::put('/books/{id}', "LibraryController@updateBook");
Route::delete('/books/{id}', "LibraryController@destroyBook");
Route::post('/books/multipledelete', "LibraryController@multipleBookDelete");
Route::get('/getissuedbooks/{id}', "LibraryController@getissuedBooksByUser");
Route::get('/getallissuedbooks', "LibraryController@getallissuedbooks");
Route::get('/getbooksforissue', "LibraryController@getbooksforissue");

Route::post('/issue-book/{book_id}/{user_id}', "LibraryController@issueBook");
Route::post('/return-book/{transaction_id}', "LibraryController@returnBook");


Route::resource('cal', 'gCalendarController');
Route::get('oauthgooglecallback', 'gCalendarController@oauth')->name('oauthCallback');

Route::middleware('auth:api')->post('/logout', "AuthController@logout");

