window.Vue = require('vue');
// Vue.prototype.$auth_id = document.querySelector("meta[name='auth_id']").getAttribute('content');
// Vue.prototype.$auth_name = document.querySelector("meta[name='auth_name']").getAttribute('content');
// Vue.prototype.$auth_email = document.querySelector("meta[name='auth_email']").getAttribute('content');
Vue.prototype.$auth_id = 1
Vue.prototype.$auth_name = "shubham"
Vue.prototype.$auth_email = "shubham@gmail.com"
Vue.mixin({
    methods: {
        
        // Load index=====================
        loadIndex(page = 1) {
            this.$Progress.start();
            axios.get('/api' + this.dataSet.path + '?page='+page)
                .then(({
                    data
                }) => {
                    // this.fetchedRows = data
                    this.fetchedRows = data.data //For pagination
                    this.dataSet.loading = false;
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: 'Oops... Something went wrong',
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            // footer: '<a href>Report this issue</a>'
                        })
                    }
                });
        },

        getResults(page = 1) {
            axios.get('/api'+this.dataSet.path+'?page=' + page)
                .then(response => {
                    this.fetchedRows = response.data;
                });
        },
        
        
        // Custom store methods==================
        customStore(defaultClass = ".ui.form", path = this.dataSet.path) {
            if (true) {
                this.$Progress.start();
                this.form.post('/api' + path)
                    .then(() => {
                        toast.fire({
                            type: 'success',
                            title: this.dataSet.toastMsg,
                        })
                        this.$Progress.finish();
                        this.$router.push(this.dataSet.path);
                    })
                    .catch((error) => {
                        console.log(error)
                        this.$Progress.fail();
                        if (error.status == 401) {
                            this.$router.push('/loginf');
                        } else {
                            if (error.status == 422) {
                                 Swal({
                                    type: 'error',
                                    title: 'Validation error',
                                    text: 'Please review the validation error messages in the fields highlighted in red',
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                })
                            }
                            else {
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: error,
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        }
                    });
            }
        },


        // Delete entry====================

        customDelete(id) {

            new Swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.form.delete('/api' + this.dataSet.path + '/' + id)
                        .then((data) => {
                            if(data.data[1]){
                                toast.fire({
                                    type: 'success',
                                    title: this.dataSet.deleteMsg,
                                })
                                this.$router.push(this.dataSet.path);
                            }
                            else{
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: data.data[0],
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        })
                        .catch((error) => {
                            if (error.response.status == 401) {
                                this.$router.push('/loginf');
                            } else {
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: error,
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        });
                }

            })

        },

        // Update entry====================

        customUpdate() {

            this.$Progress.start();
            this.form.put('/api' + this.dataSet.path + '/' + this.profileData.id)
                .then(() => {
                    Fire.$emit('loadprofile')
                    
                    toast.fire({
                        type: 'success',
                        title: this.dataSet.toastMsg,
                    })
                    this.$Progress.finish();
                    this.$router.push(this.dataSet.path);
                })
                .catch((error) => {
                    this.$Progress.fail();
                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        if (error.response.status == 422) {
                            new Swal({
                                type: 'error',
                                title: 'Validation error',
                                text: 'Please review the validation error messages in the fields highlighted in red',
                                showCloseButton: true,
                                showConfirmButton: false,
                            })
                        }
                        else {
                            new Swal({
                                type: 'error',
                                title: 'Oops... Something went wrong',
                                text: error,
                                showCloseButton: true,
                                showConfirmButton: false,
                                // footer: '<a href>Report this issue</a>'
                            })
                        }
                    }

                });

        },


        // Load single entry==========================
        
        loadProfile(id) {
            this.$Progress.start();
            axios.get('/api' + this.dataSet.path + '/' + id)
                .then(({
                    data
                }) => {
                    this.profileData = data;
                    this.form.fill(data);
                    this.dataSet.loading = false;
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: 'Oops... Something went wrong',
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            // footer: '<a href>Report this issue</a>'
                        })
                    }
                });
        },
        
        viewProfile(id) {
            this.$router.push(this.dataSet.path + '/' + id + '/general');
        },

        viewEditForm(id) {
            this.$router.push(this.dataSet.path + '/' + id + '/edit');
        },

        // Delete list items=====================
        deleteSelected() {

            new Swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.$Progress.start();
                    const checkboxids = this.dataSet.eachCheckbox;
                    const url = '/api' + this.dataSet.path + '/multipledelete';

                    axios.post(url, {
                            ids: checkboxids
                        })
                        .then(({
                            data
                        }) => {
                            Fire.$emit('loadindex')
                            if (data[2]){
                                toast.fire({
                                    type: 'success',
                                    title: data[0],
                                })
                                this.$router.push(this.dataSet.path);
                            }
                            else{
                                var items='';
                                data[1].forEach(element => {
                                    items = items+"<li>"+element+"</li>"
                                });
                                items="<ul class='swal_ul'>"+items+"</ul>";
                                new Swal({
                                    title: '<strong><u>' + data[0]+'</u></strong>',
                                    type: 'warning',
                                    html:
                                        'The following entries could not be deleted because there are non returned books: <br>' +
                                        items,
                                    showCloseButton: true,
                                    showCancelButton: false,
                                    focusConfirm: true,
                                    cancelButtonText:
                                        'Ok',
                                })
                            }
                            // this.$Progress.finish();
                        })
                        .catch((error) => {
                            this.$Progress.fail();
                            if (error.response.status == 401) {
                                this.$router.push('/loginf');
                            } else {
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: error,
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        });
                }

            })
        },

        toggleCheckbox() {
            this.selectAll = !this.selectAll
            if (this.selectAll) {
                this.fetchedRows.forEach((fetchedRow, index) => {
                    Vue.set(this.dataSet.eachCheckbox, index, fetchedRow.id)
                })
            } else {
                this.dataSet.eachCheckbox = []
            }
        },

        // Fetch - helper methods==============================
        allLibraryUsers(link){
            this.$Progress.start();
            axios.get('/api' + link)
                .then(({
                    data
                }) => {
                    this.libraryusers = data
                        // console.log(isDropdown)
                        this.libraryusers.unshift({
                            key:'',
                            value:'',
                            name:'Select'
                        });
                        // console.log(data);

                    
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        getAllIssuedBooks(){
            this.$Progress.start();
            axios.get('/api' +'/getallissuedbooks')
                .then(({
                    data
                }) => {
                    this.issuedbooks = data
                    
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        getIssuedBooks(id){
            this.$Progress.start();
            axios.get('/api' +'/getissuedbooks/'+id)
                .then(({
                    data
                }) => {
                    this.issuedbooks = data
                    
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        getBooksForIssue(){
            this.$Progress.start();
            axios.get('/api' +'/getbooksforissue')
                .then(({
                    data
                }) => {
                    this.books = data
                    
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        issueBook(book_id,selecteduserfortransaction){
            this.$Progress.start();
            axios.post('/api' +'/issue-book/'+book_id+'/'+selecteduserfortransaction)
                .then(({
                    data
                }) => {
                    
                    this.selecteduserfortransaction = null;
                    toast.fire({
                        type: 'success',
                        title: 'Book issued',
                    })
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        returnBook(transaction_id){
            this.$Progress.start();
            axios.post('/api' +'/return-book/'+transaction_id)
                .then(({
                    data
                }) => {
                    this.books = [];
                    this.selecteduserfortransaction = null;
                    toast.fire({
                        type: 'success',
                        title: 'Book returned',
                    })
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },


        customFormatter(date) {
            return this.$options.filters.customFormatter(date);
        },

        phpDate() {
            x = new Date();
            return this.$options.filters.myDateInISOString(x);
        },

        searchIt() {
            if (this.dataSet.search != '') {
                if (this.$refs.vendorfilter!=undefined){
                    this.filtername= 'Apply filters'
                    this.filterclass= 'default'
                    this.$refs.vendorfilter.form.reset();
                }
                Fire.$emit('searching');
                this.showClearFilter = true;
                this.showClearCustomSearchFilter = false;
            }
        },

        clearFilter() {
            Fire.$emit('loadindex')
            this.dataSet.search = '';
            this.showClearFilter = false;
            if (this.$refs.vendorfilter != undefined) {
                this.filtername = 'Apply filters'
                this.filterclass = 'default'
                this.$refs.vendorfilter.form.reset();

                $('#expertise').dropdown("clear");
                this.showClearCustomSearchFilter = false;

            }
        },
        pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        },
        searchQuery(query) {
            this.$Progress.start();
            axios.get('/api' + this.dataSet.path + '/find?q=' + query)
                .then((data) => {
                    this.fetchedRows = data.data;
                    this.$Progress.finish();
                })
                .catch((error) => {
                    console.log(error)
                    this.$Progress.fail();
                })
        },
        getColor(color){
            switch (color) {
                case "Open":
                    return "yellow"
                    break;
                case "Lost":
                    return "grey"
                    break;

                default:
                    break;
            }
        },
        
        isDelayed(deadline) {
            return this.$options.filters.isBeforeNow(deadline);
        },

        isAfterOneWeek(deadline) {
            return this.$options.filters.isAfter7Days(deadline);
        },

        deadlinestatuscolor(deadline) {
            if(this.isDelayed(deadline)) {
                return 'red';
            }
            else if (this.isAfterOneWeek(deadline) ){
                return 'blue'
            }
            else {
                return 'yellow'
            }
        },

        printpage(){
            window.print();
        },

       


    },
    computed: {
        loggedIn(){
            // true
            return this.$store.getters.loggedIn
          }
    },

    data: function () {
        return {
            showClearFilter: false,
        }
    },

    watch: {
        'dataSet.eachCheckbox': function (values) {
            // console.log(this.fetchedRows);
            if (values != undefined && this.fetchedRows != undefined) {
                if (values.length === this.fetchedRows.length) {
                    this.selectAll = true
                } else {
                    this.selectAll = false
                }
            }
        },
    },

    

})
