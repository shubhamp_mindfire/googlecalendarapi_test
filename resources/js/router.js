window.Vue = require('vue');

// Layouts component registration=======================
import topnav from './components/layouts/Topnav'
import sidenav from './components/layouts/Sidenav'
import fixedheaders from './components/layouts/FixedHeaders'
import aboveworkarea from './components/layouts/AboveWorkArea'
import secondnav from './components/layouts/Secondnav'
import tabledummy from './components/layouts/dummy/TableDummy'
import profiledummy from './components/layouts/dummy/ProfileDummy'
import editdummy from './components/layouts/dummy/EditDummy'
Vue.component('topnav', topnav);
Vue.component('sidenav', sidenav );
Vue.component('fixedheaders', fixedheaders );
Vue.component('aboveworkarea', aboveworkarea );
Vue.component('secondnav', secondnav );
Vue.component('tabledummy', tabledummy );
Vue.component('profiledummy', profiledummy );
Vue.component('editdummy', editdummy );


// Form inputs==========================================
// import Datepicker from 'vuejs-datepicker';
// Vue.component('datebox', Datepicker);

import Datetime from 'vue-datetime'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'
Vue.use(Datetime)



import editorbox from './components/forminputs/Editorbox'
import textbox from './components/forminputs/Textbox'
import datepicker from './components/forminputs/Datepicker'
import numbox from './components/forminputs/Numbox'
import phonebox from './components/forminputs/Phonebox'
import emailbox from './components/forminputs/Emailbox'
import dropdown from './components/forminputs/Dropdown'
import commentbox from './components/forminputs/Commentbox'
import passwordbox from './components/forminputs/Passwordbox'

Vue.component('editorbox', editorbox);
Vue.component('textbox', textbox);
Vue.component('datepicker', datepicker);
Vue.component('numbox', numbox);
Vue.component('phonebox', phonebox);
Vue.component('emailbox', emailbox);
Vue.component('dropdown', dropdown);
Vue.component('commentbox', commentbox);
Vue.component('passwordbox', passwordbox);




// Books module component registration=============
import UsersList from './components/users/Index'
import UsersNew from './components/users/New'
import UsersEdit from './components/users/Edit'

// Books module component registration=============
import BooksList from './components/books/Index'
import BooksNew from './components/books/New'
import BooksEdit from './components/books/Edit'
// Issue/Return module component registration=============
import IsrtList from './components/isrt/Index'
import IsrtIssueBook from './components/isrt/IssueBook'
import IsrtTransaction from './components/isrt/Transactions'

// Settings import=========================================






import Login from './components/auth/Login'
import Logout from './components/auth/Logout'
import NotFound from './components/error/NotFound'

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);



const routes = [

    // ERP==================================

    
    // Users module====================
    {
        path: '/',
        component: Login,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/users',
        component: UsersList,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/users/new',
        component: UsersNew,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/users/:id/edit',
        component: UsersEdit,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/users/:id/duplicate',
        component: UsersEdit,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    // Books module====================
   
    {
        path: '/books',
        component: BooksList,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/books/new',
        component: BooksNew,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/books/:id/edit',
        component: BooksEdit,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/books/:id/duplicate',
        component: BooksEdit,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    // Issue/Return module====================
   
    {
        path: '/isrt',
        component: IsrtList,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/isrt/issue-book',
        component: IsrtIssueBook,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    {
        path: '/isrt/transactions',
        component: IsrtTransaction,
        meta:{conditionalRoute:true,authrole:'erp'}
    },
    // Settings======================================

    // Settings - Languages==========================
   
    // Currencies module====================
    

    {
        path: '/login',
        component: Login,
    },
    {
        path: '/logout',
        component: Logout,
    },
    {
        path: '*',
        component: NotFound,
    },


]

export default routes;
