<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Gcal Management-test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        
        <link rel="stylesheet" href="/css/app.css?v=082020">
        <link rel="stylesheet" href="/css/style.css?v=082020">
    </head>
    <body>
        <div id="app">
            <topnav></topnav>
            <router-view></router-view>
            <vue-progress-bar></vue-progress-bar>
        </div>
        <script src="https://kit.fontawesome.com/fed897147a.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="/js/app.js?v=082020" defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- <script src="/js/main.js?v=082020"></script> -->
    </body>
</html>
