<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTransaction extends Model
{
    protected $guarded=[];

    public function bookdetail(){
        return $this->belongsTo(Book::class,'book_id')->withDefault();
    }
    public function userdetail(){
        return $this->belongsTo(LibraryUser::class,'library_user_id')->withDefault();
    }
}
