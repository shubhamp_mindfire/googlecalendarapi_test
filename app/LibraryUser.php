<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryUser extends Model
{
    protected $guarded=[];

    public function isAssociated(){
        return BookTransaction::where('library_user_id',$this->id)->whereNull('return_date')->exists();
    }
    public function booksissued(){
        return $this->belongsToMany('App\Book', 'book_transactions');
    }
}
