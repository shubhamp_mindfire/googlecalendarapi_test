<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\LibraryUser;
use App\BookTransaction;
use Illuminate\Support\Facades\DB;

class LibraryController extends Controller
{
    public function __construct(){
        // $this->middleware('auth:api');
    }
    // For user page
    public function getusers (){
        return LibraryUser::query()->orderBy('name')->paginate(20);
    }

    // list of all users For Issue/Return page
    public function alllibraryusers (){
        return LibraryUser::query()->orderBy('name')->get();
    }
    // Store a user
    public function store(Request $request){

        $this->validate($request, [
          'name'=>'required|string',
          'gender'=>'required',
          'email'=>'required|unique:library_users',
          'primary_phone'=>'required',
          'address_line1'=>'required',
          'city'=>'required',
          'street'=>'required',
          'zip'=>'required',
          'country'=>'required',
        ],
        [
          "gender.required"=>"The gender field is required",
          "email.required"=>"The currency field is required",
          "primary_phone.required"=>"The Primary Phone field is required",
          "address_line1.required"=>"The address line 1 field is required",
          "city.required"=>"The city field is required",
          "street.required"=>"The street field is required",
          "zip.required"=>"The zip field is required",
          "country.required"=>"The country field is required"
        ]);
    
        DB::transaction(function () use ($request) {
          $client_id=LibraryUser::create([
            'name'=>$request->name,
            'gender'=>$request->gender,
            'email'=>$request->email,
            'primary_phone'=>$request->primary_phone,
            'secondary_phone'=>$request->secondary_phone,
            'address_line1'=>$request->address_line1,
            'address_line2'=>$request->address_line2,
            'city'=>$request->city,
            'street'=>$request->street,
            'zip'=>$request->zip,
            'country'=>$request->country
          ])->id;
          
          $password=substr(sha1(time()), 0, 6); //6 digit alphanumeric
        // For user panel
        //   User::create([
        //     'name'=>$request->primary_email,
        //     'email'=>$request->primary_email,
        //     'role'=>'client',
        //     'client_id'=>$client_id,
        //     'password'=>bcrypt($password),
        //     'default_password'=>$password
        //   ]);
        
        });
        return response()->json(['success' => 'success'], 200);
      }
    // Update user details
      public function update($id,Request $request){
    
        $this->validate($request, [
          'name'=>'required|string',
          'gender'=>'required',
          'email'=>'required|email|unique:library_users,email,'.$id,
          'primary_phone'=>'required',
          'address_line1'=>'required',
          'city'=>'required',
          'street'=>'required',
          'zip'=>'required',
          'country'=>'required',
        ],
        [
          "gender.required"=>"The gender field is required",
          "email.required"=>"The currency field is required",
          "primary_phone.required"=>"The Primary Phone field is required",
          "address_line1.required"=>"The address line 1 field is required",
          "city.required"=>"The city field is required",
          "street.required"=>"The street field is required",
          "zip.required"=>"The zip field is required",
          "country.required"=>"The country field is required"
        ]);
    
        DB::transaction(function () use ($request,$id) {
          LibraryUser::whereId($id)->update([
            'name'=>$request->name,
            'gender'=>$request->gender,
            'email'=>$request->email,
            'primary_phone'=>$request->primary_phone,
            'secondary_phone'=>$request->secondary_phone,
            'address_line1'=>$request->address_line1,
            'address_line2'=>$request->address_line2,
            'city'=>$request->city,
            'street'=>$request->street,
            'zip'=>$request->zip,
            'country'=>$request->country
          ]);
        
        });
        return response()->json(['success' => 'success'], 200);
      }

      // For edit page of user
      public function getUserdetail($id){
        return LibraryUser::findorFail($id);
      }
      // Deleting a user
      public function destroy($id){
        if(!LibraryUser::findorFail($id)->isAssociated()){
          LibraryUser::findorFail($id)->delete();
          return ["User deleted",true];
        }
        return ['This user cannot be deleted because transactions for this user already exist',false];
      }
      // Deleting multiple users
      public function multipleDelete(Request $request){

            $notdeletedid=[];
            // DB::transaction(function () use ($request,$notdeletedid) {
            foreach($request->ids as $id){
                if(!LibraryUser::findorFail($id)->isAssociated()){
                    LibraryUser::findorFail($id)->delete();
                }
                else{
                    $notdeletedid[]=$id;
                }
            }
            // });
            if(count($notdeletedid)>0){
                $display_var= LibraryUser::whereIn('id',$notdeletedid)->select('name')->pluck('name');
                return ["User(s) could not be deleted",$display_var,false];
            }
            else{
                return ["User(s) deleted",'',true];
            }
        }

        /********************************Books section  *****************************/
        // for books page
        public function getbooks (){
            return Book::query()->orderBy('title')->paginate(20);
        }
        // store a book
        public function storebook(Request $request){
    
            $this->validate($request, [
              'title'=>'required',
              'isbn'=>'required',
              'stock'=>'required'
            ],
            [
              "title.required"=>"The title field is required",
              "isbn.required"=>"The ISBN field is required",
              "stock.required"=>"The stock field is required",
            ]);
        
            DB::transaction(function () use ($request) {
              Book::create([
                'title'=>$request->title,
                'isbn'=>$request->isbn,
                'shelf_no'=>$request->shelf_no,
                'stock'=>$request->stock,
              ]);
            
            });
            return response()->json(['success' => 'success'], 200);
          }
          // update a book
        public function updateBook($id,Request $request){
    
          $this->validate($request, [
            'title'=>'required',
            'isbn'=>'required',
            'stock'=>'required'
          ],
          [
            "title.required"=>"The title field is required",
            "isbn.required"=>"The ISBN field is required",
            "stock.required"=>"The stock field is required",
          ]);
        
            DB::transaction(function () use ($request,$id) {
              Book::whereId($id)->update([
                'title'=>$request->title,
                'isbn'=>$request->isbn,
                'shelf_no'=>$request->shelf_no,
                'stock'=>$request->stock,
              ]);
            
            });
            return response()->json(['success' => 'success'], 200);
          }
          // for book edit page
          public function getBookdetail($id){
            return Book::findorFail($id);
          }
          // Delete a book
          public function destroyBook($id){
            if(!Book::findorFail($id)->isAssociated()){
              Book::findorFail($id)->delete();
              return ["Book deleted",true];
            }
            return ['This book cannot be deleted because transactions for this book already exist',false];
          }
          // Delete multile books
          public function multipleBookDelete(Request $request){
    
                $notdeletedid=[];
                // DB::transaction(function () use ($request,$notdeletedid) {
                foreach($request->ids as $id){
                    if(!Book::findorFail($id)->isAssociated()){
                        Book::findorFail($id)->delete();
                    }
                    else{
                        $notdeletedid[]=$id;
                    }
                }
                // });
                if(count($notdeletedid)>0){
                    $display_var= Book::whereIn('id',$notdeletedid)->select('title')->pluck('title');
                    return ["Book(s) could not be deleted",$display_var,false];
                }
                else{
                    return ["Book(s) deleted",'',true];
                }
            }

    // Issue return part
    // For book transaction page to see all the issued books
    public function getallissuedbooks(){
      // return LibraryUser::find($id)->booksissued;
      return BookTransaction::with(['bookdetail','userdetail'])->orderBy('updated_at','desc')->get();
    }

    // to get all the non returned books by a user
    public function getissuedBooksByUser($id){
      // return LibraryUser::find($id)->booksissued;
      return BookTransaction::with(['bookdetail','userdetail'])->where('library_user_id',$id)->whereNull('return_date')->get();
    }


    // to get all the books available
    public function getbooksforissue(){
      return Book::where('stock','>',0)->orderBy('title')->get();
    }

    // To issue a book
    public function issueBook($book_id,$user_id){
      BookTransaction::create([
        'library_user_id'=>$user_id,
        'book_id'=>$book_id,
        'issue_date'=>date('Y-m-d'),
        'due_date'=>date('Y-m-d',strtotime("+7 day", strtotime(date('Y-m-d'))))
      ]);
      return response()->json(['success' => 'success'], 200);
    }

    // to return a book
    public function returnBook($transaction_id){
      BookTransaction::whereId($transaction_id)->update([
        'return_date'=>date('Y-m-d')
      ]);
      return response()->json(['success' => 'success'], 200);
    }
}